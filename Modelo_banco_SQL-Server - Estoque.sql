Banco SQL_Server 2017

Criação do usuário de logon
===========================


Logon
=====

Usuário: Estoque. Senha: estoque01


USE [master]
GO

/* For security reasons the login is created disabled and with a random password. */
/****** Object:  Login [Estoque]    Script Date: 28/05/2020 08:26:30 ******/
CREATE LOGIN [Estoque] WITH PASSWORD=N'Sq+QCObR3NhB3HJ07fzPMcuruXYF51SUg1XNxV1Oy78=', DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[Português (Brasil)], CHECK_EXPIRATION=ON, CHECK_POLICY=ON
GO

ALTER LOGIN [Estoque] DISABLE
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

BANCO DE DADOS
==============

USE [master]
GO

/****** Object:  Database [estoque]    Script Date: 28/05/2020 08:29:24 ******/
CREATE DATABASE [estoque]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'estoque', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\estoque.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'estoque_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\estoque_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [estoque].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [estoque] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [estoque] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [estoque] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [estoque] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [estoque] SET ARITHABORT OFF 
GO

ALTER DATABASE [estoque] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [estoque] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [estoque] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [estoque] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [estoque] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [estoque] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [estoque] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [estoque] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [estoque] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [estoque] SET  DISABLE_BROKER 
GO

ALTER DATABASE [estoque] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [estoque] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [estoque] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [estoque] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [estoque] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [estoque] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [estoque] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [estoque] SET RECOVERY FULL 
GO

ALTER DATABASE [estoque] SET  MULTI_USER 
GO

ALTER DATABASE [estoque] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [estoque] SET DB_CHAINING OFF 
GO

ALTER DATABASE [estoque] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [estoque] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [estoque] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [estoque] SET QUERY_STORE = OFF
GO

ALTER DATABASE [estoque] SET  READ_WRITE 
GO


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

TABELAS
-------


USUARIO
=======

USE [estoque]
GO

/****** Object:  Table [dbo].[Usuario]    Script Date: 28/05/2020 09:45:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Usuario](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[usuario] [varchar](50) NOT NULL,
	[senha] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



USE [estoque]
GO

INSERT INTO [dbo].[Usuario]
           ([usuario]
           ,[senha])
     VALUES ('Adm','adm01')
GO

INSERT INTO [dbo].[Usuario]
           ([usuario]
           ,[senha])
     VALUES ('Usu','usu01')
GO





PERMISSAO
=========

USE [estoque]
GO

/****** Object:  Table [dbo].[Permissão]    Script Date: 30/05/2020 17:33:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Permissão](
	[idUsuario] [int] NOT NULL,
	[idAcesso] [int] NOT NULL,
 CONSTRAINT [PK_Permissão] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC,
	[idAcesso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Permissão]  WITH CHECK ADD  CONSTRAINT [FK_Permissão_Controle_Acesso] FOREIGN KEY([idAcesso])
REFERENCES [dbo].[ControleAcesso] ([id])
GO

ALTER TABLE [dbo].[Permissão] CHECK CONSTRAINT [FK_Permissão_Controle_Acesso]
GO

ALTER TABLE [dbo].[Permissão]  WITH CHECK ADD  CONSTRAINT [FK_Permissão_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([ID])
GO

ALTER TABLE [dbo].[Permissão] CHECK CONSTRAINT [FK_Permissão_Usuario]
GO



USE [estoque]
GO

INSERT INTO [dbo].[Permissão]
           ([idUsuario]
           ,[idAcesso])
     VALUES
           (1,2)
GO


INSERT INTO [dbo].[Permissão]
           ([idUsuario]
           ,[idAcesso])
     VALUES
           (2,1)
GO




CONTROLE DE ACESSO
==================

USE [estoque]
GO

/****** Object:  Table [dbo].[ControleAcesso]    Script Date: 28/05/2020 09:46:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ControleAcesso](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[DescricaoAcesso] [varchar](50) NOT NULL,
	[tipoAcesso] [char](1) NOT NULL,
 CONSTRAINT [PK_ControleAcesso] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


USE [estoque]
GO

INSERT INTO [dbo].[ControleAcesso]
           ([DescricaoAcesso]
           ,[tipoAcesso])
     VALUES
           ('Usuario comum','U')
GO

INSERT INTO [dbo].[ControleAcesso]
           ([DescricaoAcesso]
           ,[tipoAcesso])
     VALUES
           ('Administrador do Sistema','A')
GO




PRODUTOS
========

USE [estoque]
GO

/****** Object:  Table [dbo].[Produtos]    Script Date: 28/05/2020 09:46:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Produtos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[descricao] [varchar](100) NOT NULL,
	[quantidade] [numeric](18, 0) NOT NULL,
	[valorUnitario] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Produtos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

USE [estoque]
GO

INSERT INTO [dbo].[Produtos]
           ([descricao]
           ,[quantidade]
           ,[valorUnitario])
     VALUES
           ('Notebook Dell',10,2568)
GO

INSERT INTO [dbo].[Produtos]
           ([descricao]
           ,[quantidade]
           ,[valorUnitario])
     VALUES
           ('Teclado Logithec',50,150)
GO

INSERT INTO [dbo].[Produtos]
           ([descricao]
           ,[quantidade]
           ,[valorUnitario])
     VALUES
           ('SSD 240',50,234)
GO

INSERT INTO [dbo].[Produtos]
           ([descricao]
           ,[quantidade]
           ,[valorUnitario])
     VALUES
           ('Monitor 22',100,174)
GO




MOVIMENTOS
==========

USE [estoque]
GO

/****** Object:  Table [dbo].[Movimentos]    Script Date: 28/05/2020 09:46:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Movimentos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tipoMovimento] [varchar](100) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[idProduto] [int] NOT NULL,
	[quantidade] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_Movimentos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Movimentos]  WITH CHECK ADD  CONSTRAINT [FK_Movimentos_Produtos] FOREIGN KEY([idProduto])
REFERENCES [dbo].[Produtos] ([id])
GO

ALTER TABLE [dbo].[Movimentos] CHECK CONSTRAINT [FK_Movimentos_Produtos]
GO

ALTER TABLE [dbo].[Movimentos]  WITH CHECK ADD  CONSTRAINT [FK_Movimentos_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([ID])
GO

ALTER TABLE [dbo].[Movimentos] CHECK CONSTRAINT [FK_Movimentos_Usuario]
GO






SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER TI_Movimentacao
   ON  Movimentos 
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	declare
		@tipomov	varchar(100),
		@produto	int,
		@quantidade	numeric(18,0)

	select	@tipomov = tipoMovimento,
			@produto = idProduto,
			@quantidade = quantidade from inserted

	if @tipomov = 'VENDA'
	begin
		update Produtos
		set quantidade = quantidade - @quantidade
		where id = @produto
	end
	else
	begin
		update Produtos
		set quantidade = quantidade + @quantidade
		where id = @produto
	end
	
END

GO




USE [estoque]
GO

INSERT INTO [dbo].[Movimentos]
           ([tipoMovimento]
           ,[idUsuario]
           ,[idProduto]
           ,[quantidade])
     VALUES
           ('COMPRA'
           ,1
           ,1
           ,20)
GO




USE [estoque]
GO

INSERT INTO [dbo].[Movimentos]
           ([tipoMovimento]
           ,[idUsuario]
           ,[idProduto]
           ,[quantidade])
     VALUES
           ('VENDA'
           ,1
           ,1
           ,20)
GO
