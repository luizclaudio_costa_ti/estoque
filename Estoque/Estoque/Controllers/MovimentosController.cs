﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Estoque.Models;

namespace Estoque.Controllers
{
    public class MovimentosController : Controller
    {
        private EstoqueDbContext db = new EstoqueDbContext();

        // GET: Movimentos
        public ActionResult Index()
        {
            return View(db.Movimento.ToList());
        }

        // GET: Movimentos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movimentos movimentos = db.Movimento.Include(m => m.Produtos).Include(u => u.Usuarios).FirstOrDefault(x => x.id == id);
            if (movimentos == null)
            {
                return HttpNotFound();
            }
            return View(movimentos);
        }

        // GET: Movimentos/Create
        public ActionResult Create()
        {
            ViewBag.Produtos = new SelectList
              (
                 db.Produto.ToList(),
                "ID",
                "descricao"
              );

            ViewBag.Usuario = new SelectList
              (
                 db.Usuario.ToList(),
                "id",
                "usuario"
              );

            return View();
        }

        // POST: Movimentos/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Movimentos movimentos)
        {
            if (ModelState.IsValid)
            {
                db.Movimento.Add(movimentos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(movimentos);
        }

        // GET: Movimentos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movimentos movimentos = db.Movimento.Find(id);
            if (movimentos == null)
            {
                return HttpNotFound();
            }

            ViewBag.Produtos = new SelectList
             (
                db.Produto.ToList(),
               "ID",
               "descricao"
             );

            ViewBag.Usuario = new SelectList
              (
                 db.Usuario.ToList(),
                "id",
                "usuario"
              );

            return View(movimentos);
        }

        // POST: Movimentos/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,tipoMovimento,idUsuario,idSenha,quantidade")] Movimentos movimentos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(movimentos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(movimentos);
        }

        // GET: Movimentos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movimentos movimentos = db.Movimento.Find(id);
            if (movimentos == null)
            {
                return HttpNotFound();
            }
            return View(movimentos);
        }

        // POST: Movimentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Movimentos movimentos = db.Movimento.Find(id);
            db.Movimento.Remove(movimentos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
