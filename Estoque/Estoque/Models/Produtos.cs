﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Estoque.Models
{
    [Table("Produtos")]
    public class Produtos
    {
        public int ID { get; set; }

        [Display(Name = "Descrição")]
        [Required(ErrorMessage = "A descrição do produto é obrigatório", AllowEmptyStrings = false)]
        public string descricao { get; set; }

        [Display(Name = "Quantidade")]
        [Required(ErrorMessage = "A quantidade é obrigatório", AllowEmptyStrings = false)]
        public decimal quantidade { get; set; }

        [Display(Name = "Valor Unitário")]
        [Required(ErrorMessage = "O valor unitário é obrigatório", AllowEmptyStrings = false)]
        public decimal valorUnitario { get; set; }

        public ICollection<Movimentos> Movimento { get; set; }

    }
}