﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Estoque.Models
{
    [Table("Movimentos")]
    public class Movimentos
    {        
        public int id { get; set; }

        [Display(Name = "Tipo Movimento")]
        [Required(ErrorMessage = "O tipo de movimento é obrigatório", AllowEmptyStrings = false)]
        public string tipoMovimento { get; set; }

        [Display(Name = "Usuário")]
        public int idUsuario { get; set; }

        [Display(Name = "Produto")]
        public int idProduto { get; set; }

        [Display(Name = "Quantidade")]
        [Required(ErrorMessage = "A quantidade é obrigatório", AllowEmptyStrings = false)]
        public decimal quantidade { get; set; }


        [ForeignKey("idUsuario")]
        public Usuarios Usuarios { get; set; }

        [ForeignKey("idProduto")]
        public Produtos Produtos { get; set; }

    }
}