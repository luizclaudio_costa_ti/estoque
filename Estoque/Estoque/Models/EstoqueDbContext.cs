﻿using System.Data.Entity;

namespace Estoque.Models
{
    public class EstoqueDbContext : DbContext
    {
        public EstoqueDbContext()
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<Usuarios> Usuario { get; set; }
        public DbSet<Produtos> Produto { get; set; }
        public DbSet<Permissao> Permissao { get; set; }
        public DbSet<ControleAcesso> ControleAcesso { get; set; }
        public DbSet<Movimentos> Movimento { get; set; }
    }
}