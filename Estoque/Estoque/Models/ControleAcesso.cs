﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Estoque.Models
{
    public class ControleAcesso
    {
        public int id { get; set; }

        [Display(Name = "Descrição")]
        [Required(ErrorMessage = "A descrição do controle de acesso é obrigatório", AllowEmptyStrings = false)]
        public string descricao { get; set; }

        [Display(Name = "Tipo de Acesso")]
        [Required(ErrorMessage = "O tipo de acesso é obrigatório", AllowEmptyStrings = false)]
        public string tipoAcesso { get; set; }
    }
}