﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Estoque.Models
{
    [Table("Usuario")]
    public class Usuarios
    {
        public int ID { get; set; }

        [Display(Name = "Usuario")]
        [Required(ErrorMessage = "O nome do usuário é obrigatório", AllowEmptyStrings = false)]
        public string usuario { get; set; }

        [Display(Name = "Senha")]
        [Required(ErrorMessage = "A senha do usuário é obrigatório", AllowEmptyStrings = false)]
        public string senha { get; set; }

        public ICollection<Movimentos> Movimento { get; set; }
    }
}