﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Estoque.Models
{
    public class Permissao
    {
        [Key]
        [Column(Order = 1)]
        public int idUsuario { get; set; }
        [Key]
        [Column(Order = 2)]
        public int idAcesso { get; set; }
    }
}